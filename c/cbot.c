#include <math.h>
#include <errno.h>
#include <netdb.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>

#include "cJSON.h"

int flag = 0;
int make_tree_flag=0;

static cJSON *ping_msg(int gt);
static cJSON *join_msg(char *bot_name, char *bot_key);
static cJSON *join_msg_custom(char *bot_name, char *bot_key, char *trackName, int carCount);
static cJSON *throttle_msg(double throttle, int gt);
static cJSON *make_msg(char *type, cJSON *msg, int gt);

static cJSON *read_msg(int fd);
static void write_msg(int fd, cJSON *msg);

int pre[1000];
int segs[1000];

struct SEGMENT
{
  int straight;
  int s; //switch
  int id;
  double radius;
  double angle;
  double length[16];
};

struct LANE
{
  int index;
  double distance;
};

struct CAR
{
  char name[81];
  char color[81];
  double length;
  double width;
  double guide;
};

struct TRACK
{
  struct SEGMENT segment[1000];
  struct LANE lane[16];
  struct CAR car[16];
  int total_segments;
  int total_lanes;
  char id[81];
  char name[81];
}track;

struct CAR_POSITION
{
  char name[81];
  char color[81];
  double angle;
  double piece_distance;
  double throttle;
  int piece_index;
  int start_lane;
  int end_lane;
  int lap;
  int crash;
  int crash_piece;
  double crash_distance;
  double speed;
  double acceleration;
  double ang_acc;
};

struct NODE
{
  struct NODE *left;
  struct NODE *center;
  struct NODE *right;
  double shortest_distance;
  int shortest_way;
  int my_lane;
  int segment;
  int lap;
}tree[16];

static void initialize_segment(struct SEGMENT *s);
struct RACE_INFO
{
  struct CAR_POSITION car_position[16]; 
  struct NODE root[16];
  char game_id[81];
  char mycar[81];
  int mycar_index;
  int total_cars;
  int total_laps;
  int game_tick;
  int turbo_start;
  int turbo_ticks;
  double turbo_factor;
}race;


static void capture(cJSON *msg);
static void capture_position(cJSON *msg);
static void initialize_car_position();
static void get_my_car_name(cJSON *msg);
static void get_my_car_index(char *name);
static void register_crash(cJSON *msg);
static void register_spawn(cJSON *msg);
static void register_turbo(cJSON *msg);
static cJSON *turbo_msg(int gt);
static cJSON *switch_msg(int s, int gt);
int race_commands(int *arg1, double *arg2);
int optimal_race(int *arg1, double *arg2);
int overriding_race(int *arg1, double *arg2);
double build_tree(struct NODE *n, int depth, int lane);
void print_tree(int depth, struct NODE *n);
void print_leaf(int arg1, char *s, struct NODE n);
int search_tree(int depth, int lap, int segment, int lane, struct NODE *n);
static void clean_tree(struct NODE *n);

void print_race_info()
{
  int mc = 0;
  int i;
  for(i=0; i<race.total_cars; i++)
  {
    if(strcmp(race.mycar, race.car_position[i].name)==0)
    {
      mc=i;
      break;
    }
  }
  printf("z %d \t", race.game_tick);
  printf("%d \t", race.car_position[mc].lap);
  printf("%d \t", race.car_position[mc].piece_index);
  printf("%.2f \t", race.car_position[mc].piece_distance);
  printf("%.3f \t", race.car_position[mc].speed);
  printf("%.3f \t", race.car_position[mc].acceleration);
  printf("%.3f \t", race.car_position[mc].ang_acc);
  printf("%.2f \t", race.car_position[mc].angle);
  printf("%s\n", race.car_position[mc].crash?"CRASH":"OK");
}

static void error(char *fmt, ...)
{
    char buf[BUFSIZ];

    va_list ap;
    va_start(ap, fmt);
    vsnprintf(buf, BUFSIZ, fmt, ap);
    va_end(ap);

    if (errno)
        perror(buf);
    else
        fprintf(stderr, "%s\n", buf);

    exit(1);
}

static int connect_to(char *hostname, char *port)
{
    int status;
    int fd;
    char portstr[32];
    struct addrinfo hint;
    struct addrinfo *info;

    memset(&hint, 0, sizeof(struct addrinfo));
    hint.ai_family = PF_INET;
    hint.ai_socktype = SOCK_STREAM;

    sprintf(portstr, "%d", atoi(port));

    status = getaddrinfo(hostname, portstr, &hint, &info);
    if (status != 0) error("failed to get address: %s", gai_strerror(status));

    fd = socket(PF_INET, SOCK_STREAM, 0);
    if (fd < 0) error("failed to create socket");

    status = connect(fd, info->ai_addr, info->ai_addrlen);
    if (status < 0) error("failed to connect to server");

    freeaddrinfo(info);
    return fd;
}

static void log_message(char *msg_type_name, cJSON *msg)
{
    cJSON *msg_data;

    if (!strcmp("join", msg_type_name)) {
        puts("Joined");
    } else if (!strcmp("gameStart", msg_type_name)) {
        puts("Race started");
    } else if (!strcmp("crash", msg_type_name)) {
        register_crash(msg);
        puts("Someone crashed");
    } else if (!strcmp("spawn", msg_type_name)) {
        register_spawn(msg);
        puts("Someone spawned");
    } else if (!strcmp("turboAvailable", msg_type_name)) {
        register_turbo(msg);
        puts("Turbo Available");
    } else if (!strcmp("gameEnd", msg_type_name)) {
        puts("Race ended");
    } else if (!strcmp("yourCar", msg_type_name)) {
        puts("Your car message...");
        get_my_car_name(msg);
        //char *o = cJSON_Print(msg);
    } else if (!strcmp("error", msg_type_name)) {
        msg_data = cJSON_GetObjectItem(msg, "data");
        if (msg_data == NULL)
            puts("Unknown error");
        else
            printf("ERROR: %s\n", msg_data->valuestring);
    }
}

int main(int argc, char *argv[])
{
    int start_lapsus = 20;
    int cntr;
    for(cntr=0; cntr<1000; cntr++)
      segs[cntr]=1;
    initialize_car_position();
    int sock;
    cJSON *json;

    if (argc != 5)
        error("Usage: bot host port botname botkey\nargs count: %d\n", argc);

    sock = connect_to(argv[1], argv[2]);

    json = join_msg(argv[3], argv[4]);
    //static cJSON *join_msg_custom(char *bot_name, char *bot_key, char *trackName, int carCount)
    //json = join_msg_custom(argv[3], argv[4], "france", 1);
    write_msg(sock, json);
    cJSON_Delete(json);

    while ((json = read_msg(sock)) != NULL) {
        cJSON *msg, *msg_type;
        char *msg_type_name;

        msg_type = cJSON_GetObjectItem(json, "msgType");
        if (msg_type == NULL)
            error("missing msgType field");

        msg_type_name = msg_type->valuestring;
        if (!strcmp("carPositions", msg_type_name)) {
            if(make_tree_flag==0)
            {
              get_my_car_index(race.mycar);
              make_tree_flag++;
            }
            if(make_tree_flag==1)
            {
              make_tree_flag++;
              int i;
//double build_tree(struct NODE *n, int depth, int lane);
              for(i=0; i<track.total_lanes; i++)
              {
                build_tree(&tree[i], 0, i);
              }
              print_tree(-1, NULL);
            }
            capture_position(json);
            /*
              Only one command can be executed per tick. 
              There are 4 ways that can be done to influence the race:
                 1. Change the throttle value  (val=0)
                 2. Switch lanes  (val=1)
                 3. Use the turbo throttle  (val=2)
                 4. nothing  (val=3)

              There should be a function that takes the current state of the race, and evaluates it such that it determines which of the four courses of action to take.
            */
            int arg1=0;
            double arg2=0.0;
            int action = 0;
            if(start_lapsus>0)
            {
              msg = throttle_msg(1.0, race.game_tick);
              printf("Throttle message (%.2f) at tick %d\n", 0.45, race.game_tick);
              start_lapsus--;
              write_msg(sock, msg);

              cJSON_Delete(msg);
              cJSON_Delete(json);
              continue;
            }
            action = race_commands(&arg1, &arg2);
            switch(action)
            {
              case 0:
              {
                msg = throttle_msg(0.55, race.game_tick);
                //printf("Throttle message (%.2f) at tick %d\n", 0.45, race.game_tick);
              }break;
              case 1:
              {
                  msg = switch_msg(arg1, race.game_tick);
                  //printf("Switch message (%d) at tick %d\n", arg1, race.game_tick);
                //}
              }break;
              case 2:
              {
                msg = turbo_msg(race.game_tick);
                //printf("Turbo message at tick %d\n", race.game_tick);
              }break;
              case 3:
              {
                msg = ping_msg(race.game_tick);
                //printf("Ping message at tick %d\n", race.game_tick);
              }break;
              default:
                printf("DEFAULT? We shouldn't have arrived here!!\n");
            }
        }
        else
        {
          if(!strcmp("gameInit",  msg_type_name)) 
          {
            capture(json);
          }
          log_message(msg_type_name, json);
          msg = ping_msg(race.game_tick);
        }

        write_msg(sock, msg);

        cJSON_Delete(msg);
        cJSON_Delete(json);
    }
    /*
    for(cntr=0; cntr<track.total_lanes; cntr++)
    {
      clean_tree(tree[cntr].left);
      clean_tree(tree[cntr].center);
      clean_tree(tree[cntr].right);
    }
    */
    //clean_tree(NULL);
    return 0;
}

static cJSON *ping_msg(int gt)
{
    return make_msg("ping", cJSON_CreateString("ping"), gt);
}

static cJSON *join_msg(char *bot_name, char *bot_key)
{
    cJSON *data = cJSON_CreateObject();
    cJSON_AddStringToObject(data, "name", bot_name);
    cJSON_AddStringToObject(data, "key", bot_key);

    return make_msg("join", data, -1);
}

static cJSON *join_msg_custom(char *bot_name, char *bot_key, char *trackName, int carCount)
{
    cJSON *data = cJSON_CreateObject();
    cJSON_AddStringToObject(data, "name", bot_name);
    cJSON_AddStringToObject(data, "key", bot_key);

    cJSON *data2 = cJSON_CreateObject();
    cJSON_AddStringToObject(data2, "trackName", trackName);
    cJSON_AddNumberToObject(data2, "carCount", carCount);
    
    cJSON_AddItemToObject(data2, "botId", data);

    return make_msg("joinRace", data2, -1);
}

static cJSON *throttle_msg(double throttle, int gt)
{
    return make_msg("throttle", cJSON_CreateNumber(throttle), gt);
}

static cJSON *make_msg(char *type, cJSON *data, int gt)
{
    cJSON *json = cJSON_CreateObject();
    cJSON_AddStringToObject(json, "msgType", type);
    cJSON_AddItemToObject(json, "data", data);
    if(gt>0)
      cJSON_AddNumberToObject(json, "gameTick", gt);
    return json;
}

static cJSON *read_msg(int fd)
{
    int bufsz, readsz;
    char *readp, *buf;
    cJSON *json = NULL;

    bufsz = 16;
    readsz = 0;
    readp = buf = malloc(bufsz * sizeof(char));

    while (read(fd, readp, 1) > 0) {
        if (*readp == '\n')
            break;

        readp++;
        if (++readsz == bufsz) {
            buf = realloc(buf, bufsz *= 2);
            readp = buf + readsz;
        }
    }

    if (readsz > 0) {
        *readp = '\0';
        json = cJSON_Parse(buf);
        if (json == NULL)
            error("malformed JSON(%s): %s", cJSON_GetErrorPtr(), buf);
    }
    free(buf);
    return json;
}

static void write_msg(int fd, cJSON *msg)
{
    char nl = '\n';
    char *msg_str;

    msg_str = cJSON_PrintUnformatted(msg);

    write(fd, msg_str, strlen(msg_str));
    write(fd, &nl, 1);

    free(msg_str);
}

static void get_my_car_index(char *name)
{
  int i;
  for(i=0; race.total_cars; i++)
  {
    if(strcmp(name, race.car_position[i].name)==0)
    {
      race.mycar_index = i;
    }
  }
  return;
}

static void initialize_segment(struct SEGMENT *s)
{
  s->angle = 0;
  int i;
  for(i=0; i<16; i++)
    s->length[i] = 0;
  s->radius = 0.0;
  s->s = 0; 
  s->straight = 0;
  s->id = -1;
}

static void initialize_car_position()
{
  int i;
  for(i=0; i<1000; i++)
    pre[i]=0;
 
  for(i=0; i<16; i++)
  {
    race.car_position[i].name[0]='\0';
    race.car_position[i].color[0]='\0';
    race.car_position[i].angle = 0.0;
    race.car_position[i].piece_distance = 0.0;
    race.car_position[i].throttle = 0.0;
    race.car_position[i].piece_index = 0;
    race.car_position[i].start_lane = 0;
    race.car_position[i].end_lane = 0;
    race.car_position[i].crash = 0;
    race.car_position[i].crash_piece = 0;
    race.car_position[i].crash_distance = 0.0;
    race.car_position[i].speed = 0.0;
    race.car_position[i].acceleration = 0.0;
    race.car_position[i].ang_acc = 0.0;
  }
  race.game_id[0] = '\0';
  race.mycar[0] = '\0';
  race.mycar_index=0;
  race.total_cars = 0;
  race.game_tick = 0;
  race.turbo_start = 0;
  race.turbo_ticks = 0;
  race.turbo_factor = 0.0;
}

static void capture(cJSON *msg)
{
  cJSON *json1 = cJSON_GetObjectItem(msg, "data");

  cJSON *json2 = cJSON_GetObjectItem(json1, "race");

  cJSON *json3 = cJSON_GetObjectItem(json2, "track");

  cJSON *json40 = cJSON_GetObjectItem(json3, "lanes");
  
  track.total_lanes = cJSON_GetArraySize(json40);

  int c;
  for (c=0; c<track.total_lanes; c++)
  {
    cJSON *json50 = cJSON_GetArrayItem(json40, c);

    cJSON *json51 = cJSON_GetObjectItem(json50, "distanceFromCenter");
    if(json51)
      track.lane[c].distance = json51->valuedouble;

    json51 = cJSON_GetObjectItem(json50, "index");
    if(json51)
      track.lane[c].index = json51->valueint;
    if(c!=json51->valueint)
      printf("\n\n*********\nERROR IN LANES INDEXING\n************\n\n");
  }

  cJSON *json4 = cJSON_GetObjectItem(json3, "pieces");
  
  track.total_segments = cJSON_GetArraySize(json4);

  for (c=0; c<track.total_segments; c++)
  {
    cJSON *json5 = cJSON_GetArrayItem(json4, c);

    initialize_segment(&track.segment[c]);

    track.segment[c].id = c;

    cJSON *json = cJSON_GetObjectItem(json5, "switch");
    if(json)
      track.segment[c].s = 1;

    json = cJSON_GetObjectItem(json5, "angle");
    if(json)
    {
      track.segment[c].angle = json->valuedouble;
    }

    json = cJSON_GetObjectItem(json5, "radius");
    if(json)
      track.segment[c].radius = json->valuedouble;

    json = cJSON_GetObjectItem(json5, "length");
    if(json)
    {
      int i;
      for(i=0;i<track.total_lanes;i++)
      {
        track.segment[c].length[i] = json->valuedouble;
      }
      track.segment[c].straight = 1;
    }
    else  //segment is not straight, is curved. Calculate length.
    {
      int i;
      for(i=0; i<track.total_lanes; i++)
      {
        // arc length =
        // degrees*(pi/180)*radius
        double efective_radius = track.segment[c].radius;
        if(track.segment[c].angle>0)
          efective_radius = efective_radius - track.lane[i].distance;
        else
          efective_radius = efective_radius + track.lane[i].distance;
        track.segment[c].length[i] = fabs(track.segment[c].angle)*efective_radius*(3.1415926/180.0);
      }
    }

    //DEBUG
    int i;
    for(i=0; i<track.total_lanes; i++)
    {
      printf("segment #%d:\t length: %2.f\t switch: %d\t angle: %.2f\t radius: %.2f\n", c, track.segment[c].length[i], track.segment[c].s, track.segment[c].angle, track.segment[c].radius);
    }
  }

  json3 = cJSON_GetObjectItem(json2, "raceSession");
  json4 = cJSON_GetObjectItem(json3, "laps");
  race.total_laps = json4->valueint;
  
}

static void capture_position(cJSON *msg)
{
  /*
  if(flag)
    return;
  else
    flag = 1;
  */

  cJSON *json = cJSON_GetObjectItem(msg, "data");
  race.total_cars = cJSON_GetArraySize(json);

  int c;
  for(c=0; c<race.total_cars; c++)
  {
    if(track.segment[race.car_position[c].piece_index].straight==0)
      race.car_position[c].ang_acc = race.car_position[c].speed*race.car_position[c].speed/track.segment[race.car_position[c].piece_index].radius;
    else
      race.car_position[c].ang_acc = 0.0;

    cJSON *json1 = cJSON_GetArrayItem(json, c);

    cJSON *json2 = cJSON_GetObjectItem(json1, "id");

    cJSON *json3 = cJSON_GetObjectItem(json2, "name");
    strcpy(race.car_position[c].name, json3->valuestring);

    json3 = cJSON_GetObjectItem(json2, "color");
    strcpy(race.car_position[c].color, json3->valuestring);

    json2 = cJSON_GetObjectItem(json1, "piecePosition");

    json3 = cJSON_GetObjectItem(json2, "pieceIndex");
    double speed_tmp=race.car_position[c].speed;
    int f = 0;
    if(race.car_position[c].piece_index == json3->valueint)
      f = 1;
    race.car_position[c].piece_index = json3->valueint;

    json3 = cJSON_GetObjectItem(json2, "inPieceDistance");
    if(f)
      race.car_position[c].speed = json3->valuedouble - race.car_position[c].piece_distance;
    race.car_position[c].acceleration = speed_tmp - race.car_position[c].speed;
    race.car_position[c].piece_distance = json3->valuedouble;

    json3 = cJSON_GetObjectItem(json1, "angle");
    {
      if(race.car_position[c].angle==0 && fabs(json3->valuedouble)>0.01 && track.segment[race.car_position[c].piece_index].angle!=0)
      {
        //calculate max speed
        double A = fabs(json3->valuedouble);
        double B = (360.0/(2*3.1415926))*(race.car_position[c].speed/track.segment[race.car_position[c].piece_index].radius);
        double Bs = B - A;
        double Vth = (2*3.1415926/360.0)*Bs*(track.segment[race.car_position[c].piece_index].radius);
        double Fs = Vth*Vth/(track.segment[race.car_position[c].piece_index].radius);
        printf("A: %.3f, B: %.3f, Bs: %.3f, Vth: %.3f, Fs: %.3f\n", A, B, Bs, Vth, Fs);
      }
      race.car_position[c].angle = json3->valuedouble;
    }

    json3 = cJSON_GetObjectItem(json2, "lane");

    cJSON *json4 = cJSON_GetObjectItem(json3, "startLaneIndex");
    race.car_position[c].start_lane = json4->valueint;

    json4 = cJSON_GetObjectItem(json3, "endLaneIndex");
    race.car_position[c].end_lane = json4->valueint;

    json3 = cJSON_GetObjectItem(json2, "lap");
    race.car_position[c].lap = json3->valueint;
  }

  json = cJSON_GetObjectItem(msg, "gameId");
  strcpy(race.game_id, json->valuestring);

  json = cJSON_GetObjectItem(msg, "gameTick");
  if(json!=NULL)
  {
    if(race.game_tick<(json->valueint))
      race.game_tick=json->valueint;
    //printf("\n   * gameTick captured: %d\n", json->valueint);
  }
}

static void get_my_car_name(cJSON *msg)
{
  cJSON *json = cJSON_GetObjectItem(msg, "data");
  cJSON *json1 = cJSON_GetObjectItem(json, "color");
  strcpy(race.mycar, json1->valuestring);
}

static void register_crash(cJSON *msg)
{
  printf("\nCRASH!!!");
  int gt=0;
  cJSON *json0 = cJSON_GetObjectItem(msg, "gameTick");

  if (json0!=NULL)
    gt = json0->valueint;

  cJSON *json = cJSON_GetObjectItem(msg, "data");
  cJSON *json1 = cJSON_GetObjectItem(json, "color");

  
  //buscamos el auto
  int i;
  for(i=0;i<race.total_cars;i++)
  {
    if(strcmp(race.car_position[i].color, json1->valuestring)==0)
    {
      if(gt==0)
        gt = race.game_tick; 
      race.car_position[i].crash = gt + 400;
      race.car_position[i].crash_piece = race.car_position[i].piece_index;
      race.car_position[i].crash_distance = race.car_position[i].piece_distance;
      race.car_position[i].speed = 0.0;
      race.car_position[i].acceleration = 0.0;
      race.car_position[i].ang_acc = 0.0;
      printf("DATO GUARDADO! (crash: %s : %d)\n", json1->valuestring, race.car_position[i].crash); 
      break;
    }
  }
  race.turbo_start = 0;
  race.turbo_ticks = 0;
  race.turbo_factor = 0.0;
}

static void register_spawn(cJSON *msg)
{
  int gt;
  cJSON *json0 = cJSON_GetObjectItem(msg, "gameTick");
  if (json0!=NULL)
    gt = json0->valueint;
  else 
    gt = 0;
  printf("\nspawn!!(gametick: %d)", gt);

  cJSON *json = cJSON_GetObjectItem(msg, "data");
  cJSON *json1 = cJSON_GetObjectItem(json, "color");
  
  //buscamos el auto
  int i;
  printf("SPAWN: buscando %s\n", json1->valuestring);
  for(i=0;i<race.total_cars;i++)
  {
    if(strcmp(race.car_position[i].color, json1->valuestring)==0)
    {
      printf("SPAWN difference from crash = %d\n", race.car_position[i].crash-gt);
      race.car_position[i].speed = 0.0;
      race.car_position[i].acceleration = 0.0;
      race.car_position[i].ang_acc = 0.0;
      race.car_position[i].crash_piece = 0;
      race.car_position[i].crash_distance = 0.0;
      race.car_position[i].crash = 0;
      break;
    }
  }
  race.turbo_start = 0;
  race.turbo_ticks = 0;
  race.turbo_factor = 0.0;
}

static void register_turbo(cJSON *msg)
{
  cJSON *json = cJSON_GetObjectItem(msg, "data");

  cJSON *json1 = cJSON_GetObjectItem(json, "turboDurationTicks");
  race.turbo_ticks = json1->valueint;

  json1 = cJSON_GetObjectItem(json, "turboFactor");
  race.turbo_factor = json1->valuedouble;

  //printf("TURBO  ===>   millis: %d, ticks: %d, factor: %f\n", race.turbo_start, race.turbo_ticks, race.turbo_factor);
}

static cJSON *switch_msg(int s, int gt)
{
  if(s<0)
    return make_msg("switchLane", cJSON_CreateString("Left"), gt);
  else
    return make_msg("switchLane", cJSON_CreateString("Right"), gt);
}

static cJSON *turbo_msg(int gt)
{
    return make_msg("turbo", cJSON_CreateString("brrrrrrrrrrrrrrrrr"), -1);
}

int race_commands(int *arg1, double *arg2)
{
 /*
 Only one command can be executed per tick. 
 There are 4 ways that can be taken to influence the race:
 1. Change the throttle value  (val=0)
 2. Switch lanes  (val=1)
 3. Use the turbo throttle  (val=2)
 4. nothing  (val=3)

 There should be a function that takes the current state of the race, and evaluates it such that it determines which of the four courses of action to take.

  This will be split into two sub routines:

  1. The "optimal_race" function, will analyze the track and the current state of the race, but will ignore the existence of other cars. Then, using this information, will design a strategy to complete the laps as soon as possible.

  2. The "overriding_function", will analyze the current situation of the race, taking on account the other cars. It can override the commands of the optimal_race function, such that it can gain advantage from bumping into other cars, avoiding being bumped, etc.

 */

  print_race_info();
  int ret = 0;
  ret = optimal_race(arg1, arg2);
  if(ret<0)
    ret = overriding_race(arg1, arg2);
  return ret;
}

int optimal_race(int *arg1, double *arg2)
{
  /*
  In this function, an optimal sequence of actions will be found, in order to try to get the best race time, ignoring the presence of other cars. 

  The race will be seen as a tree of nodes, and in which we try to minimize the cost of getting to the goal nodes. This calculation should be stored in an array representing the number of segments to be traveled (# of segments of circuit * number of laps). This will allow to rapidly calculate the best sequence for the current segment.

  */
  //check corresponding action to current lap and segment
  int ret;
  ret = 0;
  int lap, segment, lane;
  segment = (race.car_position[race.mycar_index].piece_index+1)%track.total_segments;
  lap = race.car_position[race.mycar_index].lap + (segment+1)/track.total_segments;
  lane = race.car_position[race.mycar_index].end_lane;
  if(segs[lap*track.total_segments+segment])
  {
    *arg1 = search_tree(0, lap, segment, lane, &tree[0]);
    segs[lap*track.total_segments+segment] = 0;   
    if(*arg1!=0)
      ret = 1;
    else
      ret = 0;
    printf("Returning \"%d\", dir:%d,  @lap: %d, segment: %d, lane: %d, condition:%d, value:%d\n", ret, ret==1?*arg1:0, lap, segment, lane, segs[lap*track.total_segments+segment], lap*track.total_segments+segment);
  }
  //int search_tree(int depth, int lap, int segment, struct NODE *n);
  /*
  if(flag==0 && ret==1)
    flag=1;
  else if(flag==1 && ret==1)
    ret=3;
  else
    flag=0;
  */
  return ret;
}

int overriding_race(int *arg1, double *arg2)
{
  return 0;
}

double build_tree(struct NODE *n, int depth, int lane)
{
  n->lap = depth/track.total_segments;
  n->segment = (depth)%track.total_segments;
  n->my_lane = lane;
  n->left = NULL;
  n->center = NULL;
  n->right = NULL;
  n->shortest_distance = 1000000.0;
  n->shortest_way = 0;

  //final node? stop!
  if (depth == track.total_segments)
  {
    //printf("TOTAL DEPTH: %d\n", depth);
    return 0;
  }
  double distance;

  //not final, let's get the accessible lanes
  //center lane, everybody has one!
  n->center = malloc(sizeof(struct NODE));
  distance = build_tree(n->center, depth+1, lane);
  if(distance < n->shortest_distance)
  {
    n->shortest_distance = distance;
    n->shortest_way = 0;//center is the shortest distance, so far
  }
  int cur_seg = depth%track.total_segments;
  //printf("CUR_SEG: %d\n", cur_seg);
  if(track.segment[cur_seg].s)
  {
    //not the leftmost lane
    if(lane!=0)
    {
      n->left = malloc(sizeof(struct NODE));
      distance = build_tree(n->left, depth+1, lane-1)+2;
      if(distance < n->shortest_distance)
      {
        n->shortest_distance = distance;
        n->shortest_way = -1;//left is the shortest distance, so far
      }
    }
    //not the rightmost lane
    if(lane!=(track.total_lanes-1))
    {
      n->right = malloc(sizeof(struct NODE));
      distance = build_tree(n->right, depth+1, lane+1)+2;
      if(distance < n->shortest_distance)
      {
        n->shortest_distance = distance;
        n->shortest_way = 1;//right is the shortest distance
      }
    }
  }
  return n->shortest_distance + track.segment[n->segment].length[n->my_lane];
}

void print_tree(int depth, struct NODE *n)
{
  if(depth>200) return;
  if(depth<0)
  {
    printf("\nTree of optimal game:\n");
    int i;
    for(i=0;i<track.total_lanes;i++)
    {
      print_tree(depth+1, &tree[i]);
      printf("\n*********************\n\n");
    }
    printf("\n");
  }
  else
  {
    print_leaf(depth, "", *n);
    int o = n->shortest_way;
    if(o<0 && n->left!=NULL)
      print_tree(depth+1, n->left);
    if(o==0 && n->center!=NULL)
      print_tree(depth+1, n->center);
    if(o>0 && n->right!=NULL)
      print_tree(depth+1, n->right);
  }
}

void print_leaf(int arg1, char *s, struct NODE n)
{
  int i;
  for(i=0; i<arg1; i++)
    printf("%s", s);
  printf("lap:%d,", n.lap);
  printf("segm:%d,", n.segment);
  printf("mylane:%d,", n.my_lane);
  printf("sh_dis:%.2f,", n.shortest_distance);
  printf("sh_way:%d", n.shortest_way);
  printf("\n");
}

int search_tree(int depth, int lap, int segment, int lane, struct NODE *n)
{
  depth=depth%track.total_segments;
  int ret = 2;
  if(n->segment == segment && n->my_lane == lane)
    ret = n->shortest_way; 
  if(ret==2 && n->left!=NULL && n->shortest_way==-1)
    ret = search_tree(depth+1, lap, segment, lane, n->left);
  if(ret==2 && n->center!=NULL && n->shortest_way==0)
    ret = search_tree(depth+1, lap, segment, lane, n->center);
  if(ret==2 && n->right!=NULL && n->shortest_way==1)
    ret = search_tree(depth+1, lap, segment, lane, n->right);

  return ret;
}

static void clean_tree(struct NODE *n)
{
  if(n->left!=NULL)
  {
    clean_tree(n->left);
    free(n->left);
  }
  if(n->center!=NULL)
  {
    clean_tree(n->center);
    free(n->center);
  }
  if(n->left!=NULL)
  {
    clean_tree(n->right);
    free(n->right);
  }
}









